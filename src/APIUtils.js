import { ACCESS_TOKEN, API_BASE_URL } from './constants.js';


const axios = require('axios');

export function login(loginRequest) {

    return axios({
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
        },
        url: API_BASE_URL + "/login",
        data: {
            'phoneNum': loginRequest.phoneNum,
             'password': loginRequest.password  
        }
    });

}

export function sendLocation(currentLongitude,currentLatitude,token) {
    console.log('api',token,currentLatitude)
    return axios({
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
             'Authorization':'Bearer' +  token,
        },
        url: API_BASE_URL + "/updateLocation",
        data: {
            'longitude': currentLongitude,
             'latitude': currentLatitude  
        }
    });

}



export function changePassword(changePasswordRequest,token) {

    return axios({
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Authorization':'Bearer' +  token,
        },
        url: API_BASE_URL + "/changePassword",
        data: {
            'currentPass': changePasswordRequest.CurrentPass,
             'newPass': changePasswordRequest.NewPass  
        }
    });

}

export function logout(token){
     return axios({
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Authorization':'Bearer' +  token,
        },
        url: API_BASE_URL + "/logout",
        data: {
           
        }
    });
}


export function getEmployeeInfo(token){
     return axios({
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Authorization':'Bearer' +  token,
        },
        url: API_BASE_URL + "/getEmployeeInfo",
        data: {
           
        }
    });
}


