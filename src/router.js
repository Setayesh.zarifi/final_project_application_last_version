import { createSwitchNavigator,createStackNavigator } 
  from 'react-navigation';
import {AsyncStorage } from 'react-native';
// import {  } from 'react-navigation-stack'
import MainScreen from './screens/MainScreen';
import LoginScreen from './screens/LoginScreen';
import SettingsScreen from './screens/SettingsScreen';
import ChangePassword from './screens/ChangePassword';



export const RealScreen = createStackNavigator(
    {
      routeThree:{ 
        screen : SettingsScreen,
         navigationOptions: {
            title:'Location App', 
          }
      }, 
      routeFour:
          {screen : MainScreen,
           navigationOptions: {
            title:'Location App', 
            headerLeft: null,
          }
          },
      routeFive:{
        screen:ChangePassword,
         navigationOptions: {
            title:'Location App', 
          }
      }
      
    },
    {
      initialRouteName: 'routeThree',
    }
  );
  


export const MySwitchNavigator = createSwitchNavigator(
    {
    routeOne: LoginScreen,
    routeTwo: RealScreen, 
    },
    {
      initialRouteName: 'routeOne',
    }
  );

// export const createRootNavigator = (isAutheticated) => {
//   console.log("isAutheticated_createRootNavigator",isAutheticated);
//   return createSwitchNavigator(
//     {
//     routeOne: LoginScreen,
//     routeTwo: RealScreen, 
//     },
//     {
//       initialRouteName: isAutheticated ? 'routeOne' : 'routeTwo',
//     },
//   );
 
// }