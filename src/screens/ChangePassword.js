import React, { Component } from 'react';
import {changePassword,getEmployeeInfo} from "../APIUtils";
import { View ,AsyncStorage,StyleSheet,Text,TextInput,TouchableOpacity,ToastAndroid,Alert } from 'react-native';
import styles from '../res/styles';

export default class ChangePassword extends Component {
         constructor(props) {
            super(props)
            this.state = {
                CurrentPass: "",
                NewPass:  "",
                licensePlate: "",
                phoneNum:""
                
            };
            this.ChangePassword = this.ChangePassword.bind(this);
        }

        onChangeText(text){
            this.setState({CurrentPass:text})
         }
  
        onChangeTextTwo(text){
            this.setState({NewPass:text})
        }

              async componentDidMount() {
                await AsyncStorage.getItem('ACCESS_TOKEN').then((value) => {
                    if(value) {
                    getEmployeeInfo(value)
                    .then( async response => {
                        console.log("response",response.data.phoneNum);
                        this.setState({licensePlate:response.data.licensePlate});
                        this.setState({phoneNum:response.data.phoneNum});
                    })
                    .catch(error => {
                        console.log(error.message)
                    Alert.alert(
                    'Sorry!',
                    'Can not access your profile right now!',
                    [
                        {text: 'OK', onPress: () => console.log('OK Pressed')},
                    ],
                    {cancelable: false},
                    );
                    
            });
                    }
                })
        }

        ChangePassword(){
              if (
        this.state.CurrentPass  === ""  ||
        this.state.NewPass  === "" 
    ){
      
      Alert.alert(
          'Fill out the form',
          'Please fill out the required data',
          [
            {text: 'OK', onPress: () => console.log('OK Pressed')},
          ],
          {cancelable: false},
        );
    }
     
    else {
      const changePasswordRequest = {
        CurrentPass: this.state.CurrentPass,
        NewPass: this.state.NewPass,
      };
         AsyncStorage.getItem('ACCESS_TOKEN').then((value) => {
          if(value) {
        changePassword(changePasswordRequest,value)
        .then( async response => {
            console.log("response",response.data.message);
           if(response.data.message === "Password Updated"){
                    Alert.alert(
                    'Password Updated',
                    'Congrats Your password is updated!',
                    [
                        {text: 'OK', onPress: () => console.log('OK Pressed')},
                    ],
                    {cancelable: false},
                 );
           }
           else{
                Alert.alert(
                    'Current password is wrong',
                    'Sorry!Current password is wrong!',
                    [
                        {text: 'OK', onPress: () => console.log('OK Pressed')},
                    ],
                    {cancelable: false},
                 );

           }
      
        })
        .catch(error => {
            console.log(error.message)
          Alert.alert(
          'Sorry!',
          'sorry can not update password right now!',
          [
            {text: 'OK', onPress: () => console.log('OK Pressed')},
          ],
          {cancelable: false},
        );
              
      });
            
          }
         });
     
    }
        }

    render() {
        return (
             <View style={styles.containerr}>
                 <Text style={styles.boldText}>phone Number:{this.state.phoneNum}</Text>
                <Text style={styles.boldText}>license Plate:{this.state.licensePlate}</Text>
                <TextInput style={styles.TextInput1}
                name="CurrentPass"
                underlineColorAndroid = 'rgba(0,0,0,0)'
                placeholder="Current Password"
                placeholderTextColor='#ffffff'
                value={this.state.CurrentPass.value}
                onChangeText={this.onChangeText.bind(this)}
                />
                <TextInput style={styles.TextInput2}
                name="NewPass"
                underlineColorAndroid = 'rgba(0,0,0,0)'
                placeholder="New Password"
                placeholderTextColor='#ffffff'
                value={this.state.NewPass.value}
                onChangeText={this.onChangeTextTwo.bind(this)}
                />
                <TouchableOpacity style={styles.touchableOpacity} onPress={this.ChangePassword}>
                    <Text style={styles.buttonText}>ChangePassword</Text>
                </TouchableOpacity>
                
              
            </View>
        )
    }
}
