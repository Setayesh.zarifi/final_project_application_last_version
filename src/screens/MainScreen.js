import React from 'react';
import BackgroundTimer from 'react-native-background-timer';
import { Toast} from 'native-base';
import styles from '../res/styles';
import strings from '../res/strings';
import colors from '../res/colors';
import {sendLocation} from "../APIUtils"
//import react in our code.
import {
  View,
  Text,
  StyleSheet,
  Image,
  PermissionsAndroid,
  Platform,
  AsyncStorage,
  TextInput,TouchableOpacity,Alert
} from 'react-native';

import Geolocation from '@react-native-community/geolocation';

export default class MainScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentLongitude: 'unknown', //Initial Longitude
      currentLatitude: 'unknown', //Initial Latitude
    };
    this.callLocation = this.callLocation.bind(this);
    this.stop = this.stop.bind(this);
    this.requestLocationPermission = this.requestLocationPermission.bind(this);
  }

  requestLocationPermission =async () => {
            try {
          const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
            {
              title: 'Location Access Required',
              message: 'This App needs to Access your location',
            },
          );
          if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                 this.callLocation();
            //To Check, If Permission is granted
            this.intervalId = BackgroundTimer.runBackgroundTimer(() => { 
              AsyncStorage.getItem('ACCESS_TOKEN').then((value) => {
          if(value) {
              sendLocation(this.state.currentLongitude,this.state.currentLatitude,value)
              .then( response => {
                      if(response.data.message === "Sorry you are not an employee!"){
                             Alert.alert(
                                'Not An Employee',
                                'Sorry you are not an employee!',
                                [
                                    {text: 'OK', onPress: () => console.log('OK Pressed')},
                                ],
                                {cancelable: false},
                            );
                            BackgroundTimer.clearInterval(this.intervalId);
                            this.props.navigation.navigate('routeThree');
                      }
                      console.log("log1",response);    
              })
              .catch(error => {
                  console.log("log3",error.message);
                Alert.alert(
                                'Server is down',
                                'Sorry can not connect to the server right now!',
                                [
                                    {text: 'OK', onPress: () => console.log('OK Pressed')},
                                ],
                                {cancelable: false},
                            );
              }).finally(function(){
                  console.log('in finally:D')
              });
            
          }
         });
             }, 
            10000);
          } else {
            alert('Permission Denied');
          }
        } catch (err) {
          alert('err', err);
          console.warn(err);
        }
  }

  componentDidMount = () => {
      this.requestLocationPermission();
  };
  

stop(){
   Geolocation.clearWatch(this.watchID);
   BackgroundTimer.clearInterval(this.intervalId);
   this.props.navigation.navigate('routeThree');
}






 callLocation() {
   if(!this){
      return;
    }
    let that = this;
    //alert("callLocation Called");
    Geolocation.getCurrentPosition(
      //Will give you the current location
      position => {
        const currentLongitude = JSON.stringify(position.coords.longitude);
       
        const currentLatitude = JSON.stringify(position.coords.latitude);
       
        that.setState({currentLongitude: currentLongitude});
        
        that.setState({currentLatitude: currentLatitude});
        
        const locationDetails = {
          longitude: currentLongitude,
          latitude: currentLatitude,
      };

 

  

        console.log(currentLongitude);
        console.log(currentLatitude);
      },
      error => alert(error.message),
      {enableHighAccuracy: false, timeout: 20000},
    );
    that.watchID = Geolocation.watchPosition(position => {
      //Will give you the location on location change
      console.log(position);
      const currentLongitude = JSON.stringify(position.coords.longitude);
     
      const currentLatitude = JSON.stringify(position.coords.latitude);
     
      that.setState({currentLongitude: currentLongitude});
     
      that.setState({currentLatitude: currentLatitude});
      
    });
 }
 
  render() {
    return (
      <View style={styles.containerr}>
        <Image
          source={{uri: 'https://png.icons8.com/dusk/100/000000/compass.png'}}
          style={{width: 100, height: 100}}
        />
        <Text style={styles.boldText}>You are Here</Text>
        <Text
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: 16,
          }}>
          Longitude: {this.state.currentLongitude}
        </Text>
        <Text
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: 16,
          }}>
          Latitude: {this.state.currentLatitude}
        </Text>
        <TouchableOpacity style={styles.touchableOpacity} onPress={this.stop}>
                    <Text style={styles.buttonText}>STOP</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
