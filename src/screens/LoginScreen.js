import React, { Component } from 'react'
import { View ,AsyncStorage,StyleSheet,Text,TextInput,TouchableOpacity,ToastAndroid,Alert,Image } from 'react-native'
import { Container, Header, Content, Form, Item, Input, Label , Button} from 'native-base';
import {login} from "../APIUtils";
import styles from '../res/styles';


export default class LoginScreen extends Component {
     constructor(props) {
      super(props)
      this.state = {
         phoneNum:  "",
      
        password:  ""
        
    };

    this.handleLogin = this.handleLogin.bind(this);
   }


   onChangeText(text){
    this.setState({phoneNum:text})
  }
  
  onChangeTextTwo(text){
    this.setState({password:text})
  }



handleLogin(event){
     event.preventDefault();
    if (
      this.state.password  === ""  ||
      this.state.phoneNum  === "" 
    ){
      Alert.alert(
          'Fill out the form',
          'Please fill out the required data',
          [
            {text: 'OK', onPress: () => console.log('OK Pressed')},
          ],
          {cancelable: false},
        );
    }
     
    else {
      const loginRequest = {
        phoneNum: this.state.phoneNum,
        password: this.state.password,
      };
      
     login(loginRequest)
        .then( async response => {      
           this.props.navigation.navigate('routeTwo');
           await AsyncStorage.setItem('ACCESS_TOKEN',response.data.access_token );
      
        })
        .catch(error => {
          Alert.alert(
          'wrong password',
          'sorry your password is not correct!',
          [
            {text: 'OK', onPress: () => console.log('OK Pressed')},
          ],
          {cancelable: false},
        );
              
      });
    }

}


    render() {
        return (
            <View style={styles.containerr}>
               <Image
                  source={require('../images/logo.png')}
                  style={{width: 150, height: 70}}
                />
                <TextInput style={styles.TextInput1}
                name="phoneNum"
                underlineColorAndroid = 'rgba(0,0,0,0)'
                placeholder="phone number"
                placeholderTextColor='#ffffff'
                value={this.state.phoneNum.value}
                onChangeText={this.onChangeText.bind(this)}
                />
                <TextInput style={styles.TextInput2}
                name="password"
                underlineColorAndroid = 'rgba(0,0,0,0)'
                placeholder="password"
                placeholderTextColor='#ffffff'
                secureTextEntry={true}
                value={this.state.password.value}
                onChangeText={this.onChangeTextTwo.bind(this)}
                />
                <TouchableOpacity style={styles.touchableOpacity} onPress={this.handleLogin} >
                    <Text style={styles.buttonText}>Login</Text>
                </TouchableOpacity>
                
              
            </View>
        

        )
    }
}

