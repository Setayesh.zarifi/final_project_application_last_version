import React, { Component } from 'react'
import { View ,AsyncStorage,StyleSheet,Text,TextInput,TouchableOpacity,ToastAndroid,Alert } from 'react-native'
import styles from '../res/styles';
import {logout , changePassword} from "../APIUtils";

export default class SettingsScreen extends Component {
     constructor(props) {
        super(props)
        this.startSendingLocaton = this.startSendingLocaton.bind(this);
        this.changePassword = this.changePassword.bind(this);
        this.logout = this.logout.bind(this);
    }
    
    startSendingLocaton(){
        console.log("called");
         this.props.navigation.navigate('routeFour');
    }
    changePassword(){
        this.props.navigation.navigate('routeFive');
    }
    logout(){
        AsyncStorage.getItem('ACCESS_TOKEN').then((value) => {
          if(value) {
                logout(value).then(async response => {
                if(response.status==200){
                     AsyncStorage.clear();
                     this.props.navigation.navigate('routeOne');
                     Alert.alert(
                    'Logged out',
                    'Successfully logged out!',
                    [
                        {text: 'OK', onPress: () => console.log('OK Pressed')},
                    ],
                    {cancelable: false},
                 );
                }

            })
          .catch(error => {
          
          });

          }})
       
    }

    render() {
        return (
            <View style={styles.containerr}>
                 <TouchableOpacity style={styles.touchableOpacity} onPress={this.startSendingLocaton}>
                    <Text style={styles.buttonText}>Start Sending Location</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.touchableOpacity} onPress={this.changePassword}>
                    <Text style={styles.buttonText}>Change Password</Text>
                </TouchableOpacity>
                 <TouchableOpacity style={styles.touchableOpacity} onPress={this.logout}>
                    <Text style={styles.buttonText}>Logout</Text>
                </TouchableOpacity>
                
            </View>
        )
    }
}
