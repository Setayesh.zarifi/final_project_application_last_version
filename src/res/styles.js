import {StyleSheet} from 'react-native';
export default StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 50,
    padding: 16,
    // backgroundColor: 'white',
  },

  
  boldText: {
    fontSize: 20,
    color: 'black',
  },
   containerr:{
        justifyContent:'center', alignItems:'center',
        backgroundColor:'#455a64',height:'100%'
    },

    TextInput1:{
        marginTop:30,paddingLeft:20,borderRadius:25,width:300,justifyContent:'center', alignItems:'center',backgroundColor:'rgba(255,255,255,0.1)'
    },

    TextInput2:{
        paddingLeft:20,borderRadius:25,marginVertical:20,width:300,justifyContent:'center', alignItems:'center',backgroundColor:'rgba(255,255,255,0.1)'
    },

    touchableOpacity:{
        width:300,backgroundColor:'rgba(255,255,255,0.3)',borderRadius:25,marginVertical:10,paddingVertical:12
    },
    inputBox:{
        width:300,
        backgroundColor:'rgba(255,255,255,0.3)',
        borderRadius:25,
        paddingHorizontal:16,
        fontSize:16,
        color:'#ffffff',
        marginVertical:10

    },
    button:{
        width:300,
        backgroundColor:'rgba(255,255,255,0.3)',
        borderRadius:25,
        marginVertical:10,
        paddingVertical:12
    },

    buttonText:{
        fontSize:16,
        fontWeight:'500',
        color:'#ffffff',
        textAlign:'center'
    },

     touchableOpacity2:{
        width:150,backgroundColor:'rgba(255,255,255,0.3)',borderRadius:25,marginVertical:10,paddingVertical:12
    }
});
